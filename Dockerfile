FROM debian

RUN apt-get update
RUN apt-get install -y python3 python3-pip

COPY ./requirements.txt /ytdl/
RUN cd /ytdl && pip3 install `cat requirements.txt`

WORKDIR /ytdl
CMD python3 server.py

COPY server.py /ytdl
