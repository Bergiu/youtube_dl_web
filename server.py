import json
from flask import Flask, request
from youtube_dl import YoutubeDL
app = Flask(__name__)


def extract_video_url(raw_url):
    youtube_dl = YoutubeDL()
    try:
        infos = youtube_dl.extract_info(raw_url, download=False)
    except:
        return None
    return infos


@app.route("/extract", methods=["GET", "POST"])
def extract():
    url = request.args.get("url")
    res = extract_video_url(url)
    return json.dumps(res)


if __name__ == "__main__":
    app.run(host = '0.0.0.0')
