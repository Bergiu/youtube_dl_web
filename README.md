This is a wrapper for the youtube-dl extract function.

# Features
- `/extract?url=<url>`
    - Replace `<url>` with your video url to get the extracted data from youtube-dl
    - This contains for example the download url of the video

# Install
- `pip3 install $(cat requirements.txt)`

# Run
- `python3 server.py`

# Docker
My docker image is at [hub.docker.com/r/bergiu/youtube_dl_web](https://hub.docker.com/r/bergiu/youtube_dl_web).

- Run it:
    - `docker run -d --name youtube_dl_web -p "80:5000" bergiu/youtube_dl_web`
